(function(){
  'use strict';

angular
  .module('app')
  .controller('StudentController', StudentController);

  StudentController.$inject = ['$state', 'exchanger'];

  function StudentController($state, exchanger){
    var vm = this;
    vm.added = false;
    vm.addStudent = addStudent;
    vm.cssClass = 'view1';

     function addStudent() {
      if(!vm.name && !vm.phone && !vm.college) return;
      console.log(vm.name, vm.phone, vm.college);
      exchanger.setStudent(vm.name, vm.phone, vm.college);
      vm.name="";
      vm.phone="";
      vm.college="";
      vm.added = true;
    }
  }

})();
