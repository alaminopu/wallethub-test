(function(){
  'use strict';

  /**
  * @desc validator directive that can validate phone number and re-formating
  * @example <div phone-validator></div>
  */
  angular
      .module('app')
      .directive('phoneValidator', phoneValidator);

  phoneValidator.$inject = ['$filter', '$browser'];

  function phoneValidator($filter, $browser) {
      var directive = {
          link: link,
          require: 'ngModel',
          restrict: 'EA'
      };
      return directive;

      function link(scope, element, attrs, ngModelCtrl){

		var listener = function() {
            var value = element.val().replace(/[^0-9]/g, '');
            element.val($filter('tel')(value, false));
        };

	    ngModelCtrl.$parsers.push(function(viewValue) {
            return viewValue.replace(/[^0-9]/g, '').slice(0,10);
        });

        ngModelCtrl.$render = function() {
            element.val($filter('tel')(ngModelCtrl.$viewValue, false));
        };

        element.bind('change', listener);
      	element.bind('keypress', function(event) {
      		var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
      		//console.log(event.which);

      		if (!(key_codes.indexOf(event.which)>=0)){
        		event.preventDefault();
      		}
      		$browser.defer(listener);
	    });
      }
  }
	
})();