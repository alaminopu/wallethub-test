(function(){
  'use strict';

angular
  .module('app')
  .controller('ListStudentsController', ListStudentsController);

  ListStudentsController.$inject = [];

  function ListStudentsController(){
    var vm = this;
    vm.cssClass = 'view1';
    vm.filterName = ['name','grade'];
    vm.order = order;

    vm.list = [
      { name : "John", grade : "A", class : "Geography" },
      { name : "Alice", grade : "B", class : "History" },
      { name : "Alice", grade : "C", class : "Geography" },
      { name : "Brian", grade : "B", class : "English" },
      { name : "Brian", grade : "A", class : "History" },
      { name : "Alice", grade : "D", class : "English" },
      { name : "John", grade : "C", class : "English" },
      { name : "John", grade : "D", class : "History" },
      { name : "Brian", grade : "A", class : "Geography" }
    ];

    function order(filterName){
      console.log(filterName);

      vm.filterName = filterName;
    }
  
  }

})();
