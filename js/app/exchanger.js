(function(){
  'use strict';

angular
  .module('app')
  .factory('exchanger', exchanger);

  exchanger.$inject = []

  function exchanger(){
      var myData = {
        name: '',
        phone: '',
        college: ''
      }
      var data = {
        getStudent: getStudent,
        setStudent: setStudent
      }
      return data;

      function getStudent(){
        return myData;
      }

      function setStudent(name,phone, college){
        myData.name = name;
        myData.phone = phone;
        myData.college = college;
      }
      
  }

})();