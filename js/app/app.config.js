(function(){
  'use strict';

angular
  .module('app.config', ['ui.router'])
  .config(config)

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function config($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/add");
      //
      // Now set up the states
      $stateProvider
        .state('add', {
          url: "/add",
          templateUrl: "partials/add.html",
          controller: 'StudentController',
          controllerAs: 'vm'
        })
        .state('show', {
          url: "/show",
          templateUrl: "partials/show.html",
          controller: 'ShowStudentController',
          controllerAs: 'vm'
        })
        .state('list', {
          url: "/list",
          views: {
            '':{
              templateUrl: "partials/list.html",
              controller: "ListStudentsController",
              controllerAs: 'vm'
            },
            'filters@list':{
              templateUrl: "partials/filters.html"
            },
            'details@list':{
              templateUrl: "partials/details.html"
            }
          }
        });

  }

})();
