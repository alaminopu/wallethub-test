(function(){
  'use strict';

angular
  .module('app')
  .controller('ShowStudentController', ShowStudentController);

  ShowStudentController.$inject = ['exchanger'];

  function ShowStudentController(exchanger) {
    var vm = this;
    vm.student = exchanger.getStudent();
    vm.cssClass = 'view1';

  }

})();
